import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import { withFocusable } from '@noriginmedia/react-spatial-navigation';
import { themeStyles, hasWebFocusableUI } from './theme';
import { Button, getScaledValue, useNavigate, useOpenURL } from 'renative';

const Label1 = (props) => {
    const navigate = useNavigate(props);

    <View style={themeStyles.screen}>
        <ScrollView contentContainerStyle={themeStyles.container2}>
           
          <Button
          style={themeStyles.button2}
          textStyle={themeStyles.buttonText}
          title="Label1"
          className="focusable"
          onPress={() => {
              navigate('Label1', {"name":"xx"})
          }}
         >
          </Button>

        </ScrollView>
    </View>
};



export default hasWebFocusableUI ? withFocusable()(Label1) : Label1;
