import React, { useState, useEffect, useRef } from 'react';
import { Text, Image, View, StyleSheet, ScrollView, PixelRatio } from 'react-native';
import { Api, Button, getScaledValue, useNavigate, useOpenURL } from 'renative';
import { withFocusable } from '@noriginmedia/react-spatial-navigation';
import Theme, { themeStyles, hasWebFocusableUI } from './theme';
import config from '../platformAssets/renative.runtime.json';


const styles = StyleSheet.create({
    appContainerScroll: {
        paddingTop: getScaledValue(50),
        flex: 1
    },

});

const FocusableView = withFocusable()(View);

const ScreenHome = (props) => {
    const [bgColor, setBgColor] = useState(Theme.color4);
    const navigate = useNavigate(props);
    const openURL = useOpenURL();
    let scrollRef;
    let handleFocus;
    let handleUp;


    return (
        <View style={themeStyles.screen}>
            <ScrollView
                style={{ backgroundColor: bgColor }}
                ref={scrollRef}
                contentContainerStyle={themeStyles.container}>
                
                <Text style={themeStyles.textH2}>
                    {config.welcomeMessage}
                </Text>

                <Button
                    style={themeStyles.button}
                    textStyle={themeStyles.buttonText}
                    title="Login"
                    className="focusable"
                    onPress={() => {
                        navigate('my-page', {"name":"xx"})
                        //setBgColor(bgColor === '#666666' ? Theme.color1 : '#666666');
                    }}
                    onBecameFocused={handleFocus}
                    onArrowPress={handleUp}
                />
                <Button
                    style={themeStyles.button}
                    textStyle={themeStyles.buttonText}
                    title="Browse"
                    className="focusable"
                    onPress={() => {
                        navigate('login', { replace: false });
                    }}
                    onBecameFocused={handleFocus}
                />
            </ScrollView>
        </View>
    );
};

export default hasWebFocusableUI ? withFocusable()(ScreenHome) : ScreenHome;
