import React, { useState, useEffect, useRef } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import { withFocusable } from '@noriginmedia/react-spatial-navigation';
import Theme, { themeStyles, hasWebFocusableUI } from './theme';
import { Button, getScaledValue, useNavigate, useOpenURL } from 'renative';
import Content from './content'
const styles = StyleSheet.create({
    appContainerScroll: {
        paddingTop: getScaledValue(50),
        flex: 1
    },

});

const FocusableView = withFocusable()(View);

const ScreenMyPage = (props) => {
    const navigate = useNavigate(props);
    const [bgColor, setBgColor] = useState(Theme.color4);

return(
    <View style={themeStyles.screen}>
        <View style={themeStyles.container2}>
        
           
          <Button
          style={themeStyles.button2}
          textStyle={themeStyles.buttonText}
          title="Label1"
          className="focusable"
          onPress={() => {
              navigate('/my-page', {"label":"comedy"})
          }}
         >
          </Button>

          
          <Button
          style={themeStyles.button2}
          textStyle={themeStyles.buttonText}
          title="Label2"
          className="focusable"
          onPress={() => {
              navigate('Label2', {"name":"xx"})
          }}
         >
             
          </Button>
          <Button
          style={themeStyles.button2}
          textStyle={themeStyles.buttonText}
          title="Label3"
          className="focusable"
          onPress={() => {
              navigate('Label3', {"name":"xx"})
          }}
         >
          </Button>
        </View>
          <Content/>
    </View>
);
};



export default hasWebFocusableUI ? withFocusable()(ScreenMyPage) : ScreenMyPage;
