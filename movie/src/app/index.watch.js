
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import ScreenHome from '../screenHome';
import ScreenMyPage from '../screenMyPage';
import Label1 from '../Label1';

const Stack = createStackNavigator();

const App = () => (
    <NavigationContainer>
        <Stack.Navigator headerMode="none" mode="modal">
            <Stack.Screen name="home" component={ScreenHome} />
            <Stack.Screen name="my-page" component={ScreenMyPage} />
            <Stack.Screen name="Label1" component={Label1} />
        </Stack.Navigator>
    </NavigationContainer>
);

export default App;
